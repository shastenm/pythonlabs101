# Move the code you previously wrote to calculate
# how many seconds are in a year into this file.
# Then execute it as a script and print the output to your console.
days_in_year = 365
hours_in_day = 24
minutes_in_day = hours_in_day * 60
seconds_in_day = minutes_in_day * 60
seconds_in_year = seconds_in_day * days_in_year
print(seconds_in_year)

def get_secs_in_year(days, hours, minutes, seconds):
    seconds_in_day = days * hours * minutes * seconds
    print(seconds_in_day)
get_secs_in_year(365, 24, 60, 60)