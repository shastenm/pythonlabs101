# If a runner runs 10 miles in 30 minutes and 30 seconds,
# What is their average speed in kilometers per hour?
# (Tip: 1 mile = 1.6 km)

def get_kilometer(mile, kilometer, minutes, seconds):
    total_seconds = (minutes * 60) + seconds
    secs_to_mile = total_seconds / mile
    minutes_per_mile = secs_to_mile / 60
    print(f"Person traveled {mile} miles in {minutes} minutes {seconds} seconds.")
    print(f"Person traveled an average of 1 mile every {minutes_per_mile} minutes.")
    projected_kilometers_per_hour = (60 / minutes_per_mile) * kilometer 
    print(f"Person will travel {projected_kilometers_per_hour} kilometers in one hour.")
get_kilometer(10, 1.6, 30, 30)

