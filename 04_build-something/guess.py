# All the text in here are Python code comments.
# A Python code comment starts with a hash symbol (#).
# Python will ignore this when running the file.
# You'll see instructions for your labs written in code comments.
# --------------------------------
# Here's your first task:
# Re-create the guess-my-number game from the course.
# Type the whole code out instead of copy-pasting.
# Typing out code, even if you just copy it, trains your coding skills!
# Write your code below:
import random
import sys 
    
num = random.randint(1, 100)
guess = None
counter = 0

while guess != num:
    guess = int(input("Guess a number between 1 and 100: "))
    counter += 1
    if guess == num:
        print("Congrats! You won. It just took", counter, "tries.")
        sys.exit()
    else:
        print("Sorry. Try again")
