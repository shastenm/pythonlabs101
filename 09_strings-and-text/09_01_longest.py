# Which of the following strings is the longest?
# Use the len() function to find out.

longest_german_word = "Donaudampfschifffahrtsgesellschaftskapitänskajütentürschnalle"
longest_hungarian_word = "Megszentségteleníthetetlenségeskedéseitekért"
longest_finnish_word = "Lentokonesuihkuturbiinimoottoriapumekaanikkoaliupseerioppilas"
strong_password = "%8Ddb^ca<*'{9pur/Y(8n}^QPm3G?JJY}\(<bCGHv^FfM}.;)khpkSYTfMA@>N"
German = len(longest_german_word) 
Hungarian = len(longest_hungarian_word)
Finnish = len(longest_finnish_word)
Password = len(strong_password)
longest_word = ("German:", German, "Hungarian:", Hungarian, "Finnish:", Finnish, "Strong Password:", Password)
print(longest_word)

