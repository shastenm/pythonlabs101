# Use string indexing and string concatenation
# to write the sentence "we see trees" only by picking
# the necessary letters from the given string.

word = "tweezers "

letters = word[1:3]
letters1 = word[-2:-7:-2]
letters2 = word[0] + word[-3] + word[3::2]
print(letters, letters1, letters2)
