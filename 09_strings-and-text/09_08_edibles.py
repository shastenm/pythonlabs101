# Extract four words of edible food items from the sentence below.
# Use only string slicing to pick them out!
# Feel free to use pen and paper to number the indices
# and find the locations quicker.
#
# What dish can you make from these ingredients? :)

s = "They grappled with their leggins before going to see the buttercups flourish."
s1 = s[5:9] + s[11]
s2 = s[25:28] + s[31]
s3 = s[-21:-14]
s4 = s[-9:-4]
print(s1, s2, s3, s4)
