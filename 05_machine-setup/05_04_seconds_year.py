# Use the interpreter to calculate how many seconds are in a year.
# Then write the code in this script file below this line.

year = 365
day = 24
hour = 60
minutes = 60
seconds = 60

seconds_in_year = seconds * minutes * hour * day * year

print("There are", seconds_in_year, "in a year.") 
