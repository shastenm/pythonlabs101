# Fahrenheit to Celsius:
# ----------------------
# Write the necessary code to convert a degree in Fahrenheit
# to Celsius and print it to the console. Use variable names!


def get_fahrenheit():
    fahrenheit = int(input("Enter the degrees in Fahrenheit: "))
    print(fahrenheit, "degrees fahrenheit is equivalent to", (fahrenheit - 32) * .5556, "Celsius.")

def get_celsius():
    celsius = int(input("Enter the degrees in Celsius: "))
    print(celsius, "degrees celsius is equivalent to", (celsius * 1.8) + 32, "Fahrenheit.")

def main():
    choice = input("Which do you want to convert? (F)ahrenheit or (C)elsius? ").upper()
    if choice == "F":
        get_fahrenheit()
    else:
        get_celsius()
main()
