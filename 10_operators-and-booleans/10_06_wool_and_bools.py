# Write a code snippet that gives a name to a `sheep`
# and uses a boolean value to define whether it has `wool` or not.

var = "sheep"
name = input("What is the sheep's name? ")

if name == "Luke":
    print(var, "has wool.")
else:
    print(var, "doesn't wool.")
